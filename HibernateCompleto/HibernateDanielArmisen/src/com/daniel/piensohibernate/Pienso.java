package com.daniel.piensohibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Pienso {
    private int idpienso;
    private String tipo;
    private String marca;
    private String nutrientes;
    private double precio;
    private Date fechacaducidad;
    private Venta venta;
    private List<Animal> animales;

    @Id
    @Column(name = "idpienso")
    public int getIdpienso() {
        return idpienso;
    }

    public void setIdpienso(int idpienso) {
        this.idpienso = idpienso;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "nutrientes")
    public String getNutrientes() {
        return nutrientes;
    }

    public void setNutrientes(String nutrientes) {
        this.nutrientes = nutrientes;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fechacaducidad")
    public Date getFechacaducidad() {
        return fechacaducidad;
    }

    public void setFechacaducidad(Date fechacaducidad) {
        this.fechacaducidad = fechacaducidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pienso pienso = (Pienso) o;
        return idpienso == pienso.idpienso &&
                Double.compare(pienso.precio, precio) == 0 &&
                Objects.equals(tipo, pienso.tipo) &&
                Objects.equals(marca, pienso.marca) &&
                Objects.equals(nutrientes, pienso.nutrientes) &&
                Objects.equals(fechacaducidad, pienso.fechacaducidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idpienso, tipo, marca, nutrientes, precio, fechacaducidad);
    }

    @ManyToOne
    @JoinColumn(name = "idpienso", referencedColumnName = "idpienso", nullable = false)
    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    @ManyToMany
    @JoinTable(name = "pienso_animal", catalog = "", schema = "piensoshibernate", joinColumns = @JoinColumn(name = "idpienso", referencedColumnName = "idpienso", nullable = false), inverseJoinColumns = @JoinColumn(name = "idanimal", referencedColumnName = "idanimal", nullable = false))
    public List<Animal> getAnimales() {
        return animales;
    }

    public void setAnimales(List<Animal> animales) {
        this.animales = animales;
    }
}
