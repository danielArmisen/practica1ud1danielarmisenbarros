package com.daniel.piensohibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Tienda {
    private int idtienda;
    private String direccion;
    private String nombre;
    private String codigopostal;
    private String telefono;
    private List<Trabajador> trabajadores;

    @Id
    @Column(name = "idtienda")
    public int getIdtienda() {
        return idtienda;
    }

    public void setIdtienda(int idtienda) {
        this.idtienda = idtienda;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "codigopostal")
    public String getCodigopostal() {
        return codigopostal;
    }

    public void setCodigopostal(String codigopostal) {
        this.codigopostal = codigopostal;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return idtienda == tienda.idtienda &&
                Objects.equals(direccion, tienda.direccion) &&
                Objects.equals(nombre, tienda.nombre) &&
                Objects.equals(codigopostal, tienda.codigopostal) &&
                Objects.equals(telefono, tienda.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idtienda, direccion, nombre, codigopostal, telefono);
    }

    @OneToMany(mappedBy = "tienda")
    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }
}
