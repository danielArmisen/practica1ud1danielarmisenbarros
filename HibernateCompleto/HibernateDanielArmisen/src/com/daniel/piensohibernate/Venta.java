package com.daniel.piensohibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Venta {
    private double coste;
    private int id;
    private Trabajador trabajador;
    private List<Pienso> piensos;

    @Basic
    @Column(name = "coste")
    public double getCoste() {
        return coste;
    }

    public void setCoste(double coste) {
        this.coste = coste;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return Double.compare(venta.coste, coste) == 0 &&
                id == venta.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coste, id);
    }

    @ManyToOne
    @JoinColumn(name = "idtrabajador", referencedColumnName = "idtrabajador", nullable = false)
    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    @OneToMany(mappedBy = "venta")
    public List<Pienso> getPiensos() {
        return piensos;
    }

    public void setPiensos(List<Pienso> piensos) {
        this.piensos = piensos;
    }
}
