package com.daniel.piensohibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Animal {
    private int idanimal;
    private String tipo;
    private String nombre;
    private String tamanio;
    private Date edad;
    private String raza;
    private List<Pienso> piensos;
    private List<DuenioAnimal> duenioanimales;

    @Id
    @Column(name = "idanimal")
    public int getIdanimal() {
        return idanimal;
    }

    public void setIdanimal(int idanimal) {
        this.idanimal = idanimal;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "tamanio")
    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    @Basic
    @Column(name = "edad")
    public Date getEdad() {
        return edad;
    }

    public void setEdad(Date edad) {
        this.edad = edad;
    }

    @Basic
    @Column(name = "raza")
    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return idanimal == animal.idanimal &&
                Objects.equals(tipo, animal.tipo) &&
                Objects.equals(nombre, animal.nombre) &&
                Objects.equals(tamanio, animal.tamanio) &&
                Objects.equals(edad, animal.edad) &&
                Objects.equals(raza, animal.raza);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idanimal, tipo, nombre, tamanio, edad, raza);
    }

    @ManyToMany(mappedBy = "animales")
    public List<Pienso> getPiensos() {
        return piensos;
    }

    public void setPiensos(List<Pienso> piensos) {
        this.piensos = piensos;
    }

    @OneToMany(mappedBy = "animal")
    public List<DuenioAnimal> getDuenioanimales() {
        return duenioanimales;
    }

    public void setDuenioanimales(List<DuenioAnimal> duenioanimales) {
        this.duenioanimales = duenioanimales;
    }
}
