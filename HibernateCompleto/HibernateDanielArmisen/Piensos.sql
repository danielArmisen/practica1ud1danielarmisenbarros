CREATE DATABASE if not exists piensoshibernate;
--
USE piensoshibernate;
--
create table if not exists duenios(
idduenio int auto_increment primary key,
nombre varchar(50) not null,
apellidos varchar(150) not null,
dni varchar(9) not null unique,
fechanacimiento date,
pais varchar(50));
--
create table if not exists animal (
idanimal int auto_increment primary key,
tipo varchar(30) not null,
nombre varchar(100) not null,
tamanio varchar(100) not null,
edad date,
raza varchar(100));
--
create table if not exists pienso(
idpienso int auto_increment primary key,
tipo varchar(50) not null,
marca varchar(40) not null,
nutrientes varchar(40) not null,
idanimal int not null,
idduenio int not null,
precio float not null,
fechacaducidad date);

create table if not exists tienda (
idtienda int auto_increment primary key,
direccion varchar(40) not null,
nombre varchar(40) not null,
codigopostal varchar(6) not null,
telefono varchar(9) not null unique);

create table if not exists trabajador(
idtrabajador int auto_increment primary key,
sueldo double not null,
nombre varchar(40) not null,
apellidos varchar(40) not null,
dni varchar(9) not null unique,
fechanacimiento date not null);

create table if not exists venta(
coste double not null,
idtrabajador int unsigned references trabajador,
idpienso int unsigned references pienso,
primary key(idtrabajador,idpienso));

create table if not exists duenio_animal(
nombre varchar(40) not null,
fecha_nac date not null,
idanimal int unsigned references animal,
idduenio int unsigned references duenio,
primary key(idanimal,idduenio));

create table if not exists pienso_animal(
idpienso int unsigned references pienso,
idanimal int unsigned references animal,
primary key(idpienso,idanimal));
