package com.danielAB.piensos.gui;

import com.danielAB.piensos.base.Pienso;
import com.danielAB.piensos.base.PiensoGatos;
import com.danielAB.piensos.base.PiensoPerros;
import com.danielAB.piensos.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class PiensosControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Vista vista;
    private PiensosModelo piensoModelo;
    private File ruta;

    /**
     * Constructor de la clase que recibe por parametros un objeto de la clase vista y un objeto de la clase Piensosmodelo
     * Y usa el metodo cargarDatosconf
     * Añadimos los listener necesarios para realizar las demas operaciones
     * @param vista
     * @param piensosModelo
     */
    public PiensosControlador(Vista vista, PiensosModelo piensosModelo){

        this.vista = vista;
        this.piensoModelo = piensosModelo;

        try {
            cargarDatosConf();
        } catch (IOException e) {
            System.out.println("No se encuentra el fichero de configuracion " + e.getMessage());
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    /**
     * Metodo que crea el fichero Pienso.conf
     * El cual tendra la ultima ruta exportada para realizar la importacion y exportacion de los documentos
     * @throws IOException
     */
    private void cargarDatosConf() throws IOException {

        Properties p = new Properties();
        p.load(new FileReader("Pienso.conf"));
        this.ruta = new File(p.getProperty("ultimaRuta"));
    }

    /**
     * Metodo set de la ruta
     * Cambia el valor de la ruta
     * @param ruta
     */
    private void actualizarDatosConf (File ruta) {

        this.ruta = ruta;

    }

    /**
     * Metodo que da valor a la ultima ruta en el fichero Pienso.conf
     * @throws IOException
     */
    private void guardarDatosConf () throws IOException {

        Properties conf = new Properties();
        conf.setProperty("ultimaRuta",this.ruta.getAbsolutePath());

        conf.store(new PrintWriter("Pienso.conf"), "Datos conf Pienso");
    }

    /**
     * Metodo que otorga un listener a la ventana de la interfaz
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Metodo que otorga la capacidad de seleccionar un elemento de la lista de la interfaz
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Metodo que da funcionalidad a los botones y radioButtons de la interfaz
     * @param listener
     */
    private void addActionListener(ActionListener listener) {

        vista.gatoRadioButton.addActionListener(listener);
        vista.perroRadioButton.addActionListener(listener);
        vista.radioButtonComplemento1.addActionListener(listener);
        vista.radioButtonComplemento2.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.cerrarButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);

    }

    /**
     * Metodo que Utiliza el metodo de mensajeConfirmacion de la clase util y le pasa por parametros 2 String s
     * Dependiendo de la respuesta mandara un int 0 o 1 y dependiendo de la respuesta cerrara la ventana o no
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {

        int respuesta = Util.mensajeConfirmacion("¿Está seguro de querer salir?", "salir");
        if (respuesta==JOptionPane.OK_OPTION) {
            try {
                guardarDatosConf();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);
        }

    }

    /**
     * Metodo que cambia los valores de los diferentes componentes de la interfaz al seleccionar un elemento de la lista
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {

            Pienso piensoSeleccionado = (Pienso) vista.list1.getSelectedValue();
            if (piensoSeleccionado instanceof PiensoGatos) {

                vista.gatoRadioButton.doClick();
                vista.radioButtonComplemento1.setText("Taurina");
                vista.radioButtonComplemento2.setText("Leche");
                PiensoGatos pg = (PiensoGatos) vista.list1.getSelectedValue();
                if (pg.getCantidadTaurina()==true){
                    vista.radioButtonComplemento1.doClick();

                }else {
                    vista.radioButtonComplemento2.doClick();
                }
            }else {

                vista.perroRadioButton.doClick();
                vista.radioButtonComplemento1.setText("Ceninzas");
                vista.radioButtonComplemento2.setText("Grasas");
                PiensoPerros pr = (PiensoPerros) vista.list1.getSelectedValue();
                if (pr.getCantidadCenizas()==true){
                    vista.radioButtonComplemento1.doClick();

                }else {
                    vista.radioButtonComplemento2.doClick();
                }
            }
            vista.txtPeso.setText(String.valueOf(piensoSeleccionado.getPesoKG()));
            vista.txtCalorias.setText(String.valueOf(piensoSeleccionado.getCalorias()));
            vista.txtProteinas.setText(String.valueOf(piensoSeleccionado.getCantidadProteinas()));
            vista.txtCarne.setText(String.valueOf(piensoSeleccionado.getTipoCarne()));
            vista.txtPrecio.setText(String.valueOf(piensoSeleccionado.getPrecio()));
            vista.txtNombre.setText(piensoSeleccionado.getAnimal().getNombre());
            vista.txtEdad.setText(String.valueOf(piensoSeleccionado.getAnimal().getEdad()));
            vista.txtRaza.setText(piensoSeleccionado.getAnimal().getRaza());
            vista.slider1.setValue(piensoSeleccionado.getAnimal().getTamaño());
            vista.fechaNac.setDate(piensoSeleccionado.getAnimal().getFechaNacimiento());


        }

    }

    /**
     * Metodo que da fncionalidad a los botones dependiendo del caso seleccionado
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String accion = e.getActionCommand();

        switch (accion) {

            case "Nuevo":
                if (camposVacios()==true){
                    Util.mensajeError("No pueden estar vacios ninguno de los siguientes campos:\n " +
                            "Peso \n Calorias \n Cantidad de Proteinas \n Carne \n Precio \n Nombre \n Edad \n Raza \n Fecha de Nacimiento");
                    break;
                }
                if (piensoModelo.existeNombre(vista.txtNombre.getText())) {
                    Util.mensajeError("Ya existe este animal = " + vista.txtNombre.getText());
                    break;
                }
                if (vista.gatoRadioButton.isSelected()){
                    if (vista.radioButtonComplemento1.isSelected()) {
                        piensoModelo.altaPiensoGato("Gato",
                                Integer.parseInt(vista.txtPeso.getText()),
                                Double.parseDouble(vista.txtCalorias.getText()),
                                vista.txtCarne.getText(),
                                Double.parseDouble(vista.txtProteinas.getText()),
                                Double.parseDouble(vista.txtPrecio.getText()),
                                vista.txtNombre.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.slider1.getValue(),
                                false,
                                true,
                                vista.txtRaza.getText(),
                                vista.fechaNac.getDate());
                    }else {
                        piensoModelo.altaPiensoGato("Gato",
                                Integer.parseInt(vista.txtPeso.getText()),
                                Double.parseDouble(vista.txtCalorias.getText()),
                                vista.txtCarne.getText(),
                                Double.parseDouble(vista.txtProteinas.getText()),
                                Double.parseDouble(vista.txtPrecio.getText()),
                                vista.txtNombre.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.slider1.getValue(),
                                true,
                                false,
                                vista.txtRaza.getText(),
                                vista.fechaNac.getDate());
                    }
                }else {
                    if (vista.radioButtonComplemento1.isSelected()) {
                        piensoModelo.altaPiensoPerro("Perro",
                                Integer.parseInt(vista.txtPeso.getText()),
                                Double.parseDouble(vista.txtCalorias.getText()),
                                vista.txtCarne.getText(),
                                Double.parseDouble(vista.txtProteinas.getText()),
                                Double.parseDouble(vista.txtPrecio.getText()),
                                vista.txtNombre.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.slider1.getValue(),
                                true,
                                false,
                                vista.txtRaza.getText(),
                                vista.fechaNac.getDate());
                    }else {
                        piensoModelo.altaPiensoPerro("Perro",
                                Integer.parseInt(vista.txtPeso.getText()),
                                Double.parseDouble(vista.txtCalorias.getText()),
                                vista.txtCarne.getText(),
                                Double.parseDouble(vista.txtProteinas.getText()),
                                Double.parseDouble(vista.txtPrecio.getText()),
                                vista.txtNombre.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.slider1.getValue(),
                                false,
                                true,
                                vista.txtRaza.getText(),
                                vista.fechaNac.getDate());
                    }
                }
                limpiar();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFichero(ruta, "Archivos XML","xml");
                int opcion = selectorFichero.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    try {
                        piensoModelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                 refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ruta, "Archivos XML", "xml");
                int opcion2 = selectorFichero2.showSaveDialog(null);
                if (opcion2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        piensoModelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConf(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException | TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Perro":
                vista.radioButtonComplemento1.setText("Cenizas");
                vista.radioButtonComplemento2.setText("Grasas");
                break;
            case "Gato":
                vista.radioButtonComplemento1.setText("Taurina");
                vista.radioButtonComplemento2.setText("Leche");
                break;
            case "Cerrar":
                int respuesta = Util.mensajeConfirmacion("¿Está seguro de querer salir?", "salir");
                if (respuesta==JOptionPane.OK_OPTION) {
                    try {
                        guardarDatosConf();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    System.exit(0);
                }
                break;
        }


    }

    /**
     * Metodo limpiar
     * Realiza un cambio a null de todos los valores de los atributos
     */
    private void limpiar () {

        vista.txtPeso.setText(null);
        vista.txtCalorias.setText(null);
        vista.txtProteinas.setText(null);
        vista.txtCarne.setText(null);
        vista.txtPrecio.setText(null);
        vista.txtNombre.setText(null);
        vista.txtEdad.setText(null);
        vista.txtRaza.setText(null);
        vista.fechaNac.setText(null);

    }

    /**
     * Metodo que comprueba si hay algun campo obligatorio de rellenar que esta vacio
     * @return
     */
    public boolean camposVacios(){

        if (vista.txtPeso.getText().isEmpty() ||
            vista.txtCalorias.getText().isEmpty() ||
            vista.txtProteinas.getText().isEmpty() ||
            vista.txtCarne.getText().isEmpty() ||
            vista.txtPrecio.getText().isEmpty() ||
            vista.txtNombre.getText().isEmpty() ||
            vista.txtEdad.getText().isEmpty() ||
            vista.txtRaza.getText().isEmpty() ||
            vista.fechaNac.getText().isEmpty()) {

            return true;

        }
        return false;
    }

    /**
     * Metodo que actualiza la lista de piensos
     */
    public void refrescar () {
        vista.pienso.clear();
        for (Pienso p: piensoModelo.listarPiensos()) {
            vista.pienso.addElement(p);
        }

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
