package com.danielAB.piensos.gui;

import com.danielAB.piensos.base.Animal;
import com.danielAB.piensos.base.Pienso;
import com.danielAB.piensos.base.PiensoGatos;
import com.danielAB.piensos.base.PiensoPerros;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase PiensosModelo
 */
public class PiensosModelo {

    private ArrayList<Pienso> listaPiensos;

    /**
     * Constructor de la clase PiensoModelo
     */
    public PiensosModelo () {
        listaPiensos = new ArrayList<Pienso>();
    }

    /**
     *Metodo que devuelve la lista de piensos
     * @return
     */
    public ArrayList<Pienso> listarPiensos() {
        return listaPiensos;
    }

    /**
     * Metodo que returna un boolean y recibe un String por parametro
     * Recorre la lista y si existe un valor igual al que recibe por parametro returna true o false
     * @param nombre
     * @return
     */
    public boolean existeNombre(String nombre) {

        for (Pienso p : listaPiensos){
            if (p.getAnimal().getNombre().equals(nombre)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo de altaPiensoGato, usa el constructor de la clase Pienso gato y rellena todos los campos
     * @param animal
     * @param peso
     * @param calorias
     * @param carne
     * @param proteinas
     * @param precio
     * @param nombre
     * @param edad
     * @param tamaño
     * @param taurina
     * @param leche
     * @param raza
     * @param fechaNacimiento
     */
    public void altaPiensoGato(String animal,int peso, double calorias, String carne, double proteinas, double precio, String nombre, int edad, int tamaño,boolean taurina,boolean leche ,String raza, LocalDate fechaNacimiento) {

        PiensoGatos pg = new PiensoGatos(altaAnimal(animal,nombre,edad,tamaño,raza,fechaNacimiento),peso,calorias,precio,proteinas,carne,leche,taurina);
        listaPiensos.add(pg);
    }

    /**
     * Metodo AltaPiensoPerro, usa el constructor de la clase PiensoPerro y rellena todos los campos
     * @param animal
     * @param peso
     * @param calorias
     * @param carne
     * @param proteinas
     * @param precio
     * @param nombre
     * @param edad
     * @param tamaño
     * @param cenizas
     * @param grasas
     * @param raza
     * @param fechaNacimiento
     */
    public void altaPiensoPerro(String animal, int peso, double calorias, String carne, double proteinas, double precio, String nombre, int edad, int tamaño, boolean cenizas, boolean grasas, String raza, LocalDate fechaNacimiento) {

        PiensoPerros pr = new PiensoPerros(altaAnimal(animal,nombre,edad,tamaño,raza,fechaNacimiento),  peso,  calorias,  precio,  proteinas,  carne,  grasas, cenizas);
        listaPiensos.add(pr);
    }

    /**
     * Metodo alta animal, da valor a traves del constructor de la clase a todos los valores de esta
     * @param animal
     * @param nombre
     * @param edad
     * @param tamaño
     * @param raza
     * @param fechaNacimiento
     * @return
     */
    public Animal altaAnimal(String animal,String nombre,int edad,int tamaño,String raza, LocalDate fechaNacimiento) {

        Animal a = new Animal(animal,nombre, edad, tamaño, raza, fechaNacimiento);
        return a;
    }

    /**
     * Metodo importarXML, recibe un fichero por parametro
     * Lee un fichero xml y recoge de este y los introduce en los atributos dependiendo si es una instancia de PiensoPerro o PiensoGato, al recorrer la lista de nodos
     * A partir de aqui con los datos adquiridos del fichero xml, los introduce en las variables de los objetos de las correspondientes clases, y los añade al arraylist listaPiensos
     * @param selectedFile
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File selectedFile) throws ParserConfigurationException, IOException, SAXException {

        listaPiensos = new ArrayList<Pienso>();
        PiensoGatos pg = null;
        PiensoPerros pr = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(selectedFile);

        NodeList lista = documento.getElementsByTagName("*");

        for (int i=0;i<lista.getLength();i++) {
            Element nodoPienso = (Element) lista.item(i);

            if (nodoPienso.getTagName().equals("PiensoPerro")) {

                pr = new PiensoPerros();
                pr.setPesoKG(Double.parseDouble(nodoPienso.getChildNodes().item(0).getTextContent()));
                pr.setCalorias(Double.parseDouble(nodoPienso.getChildNodes().item(1).getTextContent()));
                pr.setCantidadProteinas(Double.parseDouble(nodoPienso.getChildNodes().item(2).getTextContent()));
                pr.setTipoCarne(nodoPienso.getChildNodes().item(3).getTextContent());
                pr.setPrecio(Double.parseDouble(nodoPienso.getChildNodes().item(4).getTextContent()));
                pr.setCantidadCenizas(Boolean.parseBoolean(nodoPienso.getChildNodes().item(5).getTextContent()));
                pr.setCantidadGrasas(Boolean.parseBoolean(nodoPienso.getChildNodes().item(6).getTextContent()));
                pr.getAnimal().setNombre(nodoPienso.getChildNodes().item(7).getTextContent());
                pr.getAnimal().setEdad(Integer.parseInt(nodoPienso.getChildNodes().item(8).getTextContent()));
                pr.getAnimal().setTamaño(Integer.parseInt(nodoPienso.getChildNodes().item(9).getTextContent()));
                pr.getAnimal().setRaza(nodoPienso.getChildNodes().item(10).getTextContent());
                pr.getAnimal().setFechaNacimiento(LocalDate.parse(nodoPienso.getChildNodes().item(11).getTextContent()));
                listaPiensos.add(pr);
            }else if (nodoPienso.getTagName().equals("PiensoGato")) {

                pg = new PiensoGatos();
                pg.setPesoKG(Double.parseDouble(nodoPienso.getChildNodes().item(0).getTextContent()));
                pg.setCalorias(Double.parseDouble(nodoPienso.getChildNodes().item(1).getTextContent()));
                pg.setCantidadProteinas(Double.parseDouble(nodoPienso.getChildNodes().item(2).getTextContent()));
                pg.setTipoCarne(nodoPienso.getChildNodes().item(3).getTextContent());
                pg.setPrecio(Double.parseDouble(nodoPienso.getChildNodes().item(4).getTextContent()));
                pg.setCantidadTaurina(Boolean.parseBoolean(nodoPienso.getChildNodes().item(5).getTextContent()));
                pg.setLeche(Boolean.parseBoolean(nodoPienso.getChildNodes().item(6).getTextContent()));
                pg.getAnimal().setNombre(nodoPienso.getChildNodes().item(7).getTextContent());
                pg.getAnimal().setEdad(Integer.parseInt(nodoPienso.getChildNodes().item(8).getTextContent()));
                pg.getAnimal().setTamaño(Integer.parseInt(nodoPienso.getChildNodes().item(9).getTextContent()));
                pg.getAnimal().setRaza(nodoPienso.getChildNodes().item(10).getTextContent());
                pg.getAnimal().setFechaNacimiento(LocalDate.parse(nodoPienso.getChildNodes().item(11).getTextContent()));
                listaPiensos.add(pg);

            }

        }



    }

    /**
     * Metodo exportar que recibe un fichero por paramtro
     * Crea un documento xml en este caso ya que le pasamos por parametro la sentencia xml
     * Creamos el elemento raiz a partir del documento creado, le damos nombre y creamos dos nodos, con una condicion discriminamos si es Pienso de perros o gatos
     * a partir de aqui introducimos los datos a dicho nodo con otra instancia de nodo "nodoDatos"
     * @param selectedFile
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File selectedFile) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml",null);

        Element raiz = documento.createElement("Piensos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoPienso = null;
        Element nodoDatos = null;
        Text txt = null;

        for (Pienso unPienso: listaPiensos) {

            if (unPienso instanceof PiensoPerros) {
                nodoPienso = documento.createElement("PiensoPerro");
            }else {
                nodoPienso = documento.createElement("PiensoGato");
            }
            raiz.appendChild(nodoPienso);

            nodoDatos = documento.createElement("Peso");
            nodoPienso.appendChild(nodoDatos);
            txt = documento.createTextNode(String.valueOf(unPienso.getPesoKG()));
            nodoDatos.appendChild(txt);

            nodoDatos = documento.createElement("Calorias");
            nodoPienso.appendChild(nodoDatos);
            txt = documento.createTextNode(String.valueOf(unPienso.getCalorias()));
            nodoDatos.appendChild(txt);

            nodoDatos = documento.createElement("Proteinas");
            nodoPienso.appendChild(nodoDatos);
            txt = documento.createTextNode(String.valueOf(unPienso.getCantidadProteinas()));
            nodoDatos.appendChild(txt);

            nodoDatos = documento.createElement("Carne");
            nodoPienso.appendChild(nodoDatos);
            txt = documento.createTextNode(unPienso.getTipoCarne());
            nodoDatos.appendChild(txt);

            nodoDatos = documento.createElement("Precio");
            nodoPienso.appendChild(nodoDatos);
            txt = documento.createTextNode(String.valueOf(unPienso.getPrecio()));
            nodoDatos.appendChild(txt);

            if (unPienso instanceof PiensoPerros) {

                nodoDatos = documento.createElement("Cenizas");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(((PiensoPerros) unPienso).getCantidadCenizas()));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Grasas");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(((PiensoPerros) unPienso).getCantidadGrasas()));
                nodoDatos.appendChild(txt);

            }else {

                nodoDatos = documento.createElement("Taurina");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(String.valueOf(((PiensoGatos) unPienso).getCantidadTaurina())));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Leche");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(String.valueOf(((PiensoGatos) unPienso).getLeche())));
                nodoDatos.appendChild(txt);

            }

            if (unPienso instanceof PiensoPerros && unPienso.getAnimal().getAnimal().equalsIgnoreCase("Perro")) {

                nodoDatos = documento.createElement("Nombre");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(unPienso.getAnimal().getNombre());
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Edad");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getEdad()));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Tamaño");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getTamaño()));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Raza");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(unPienso.getAnimal().getRaza());
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("FechaNacimiento");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getFechaNacimiento()));
                nodoDatos.appendChild(txt);

            }else if (unPienso instanceof PiensoGatos && unPienso.getAnimal().getAnimal().equalsIgnoreCase("Gato")) {

                nodoDatos = documento.createElement("Nombre");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(unPienso.getAnimal().getNombre());
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Edad");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getEdad()));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Tamaño");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getTamaño()));
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("Raza");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(unPienso.getAnimal().getRaza());
                nodoDatos.appendChild(txt);

                nodoDatos = documento.createElement("FechaNacimiento");
                nodoPienso.appendChild(nodoDatos);
                txt = documento.createTextNode(String.valueOf(unPienso.getAnimal().getFechaNacimiento()));
                nodoDatos.appendChild(txt);

            }

            Source source = new DOMSource(documento);
            Result result = new StreamResult(selectedFile);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);


        }

    }
}
