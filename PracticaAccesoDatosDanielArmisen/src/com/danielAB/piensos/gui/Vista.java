package com.danielAB.piensos.gui;

import com.danielAB.piensos.base.Pienso;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Clase Vista
 */
public class Vista {
    public JPanel panel1;
    public JButton nuevoButton;
    public JButton exportarButton;
    public JButton importarButton;
    public JList list1;
    public JTextArea txtNombre;
    public JTextArea txtEdad;
    public JRadioButton perroRadioButton;
    public JRadioButton gatoRadioButton;
    public JTextArea txtRaza;
    public JButton cerrarButton;
    public JSlider slider1;
    public JTextArea txtPeso;
    public JTextArea txtCalorias;
    public JTextArea txtProteinas;
    public JTextArea txtCarne;
    public JTextArea txtPrecio;
    public JRadioButton radioButtonComplemento1;
    public JRadioButton radioButtonComplemento2;
    public  DatePicker fechaNac;
    public JFrame frame;

    public DefaultListModel<Pienso> pienso;

    /**
     * Constructor de la Clase
     */
    public Vista() {

        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        //Llamamos al metodo componentes
        componentes();

    }

    /**
     * Metodo componentes, inicializamos la lista pienso, y la pasamos como parametro a la lista de la interfaz
     */
    public void componentes(){

        pienso = new DefaultListModel<Pienso>();
        list1.setModel(pienso);

    }

}
