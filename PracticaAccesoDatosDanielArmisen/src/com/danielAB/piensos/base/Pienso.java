package com.danielAB.piensos.base;

/**
 * Clase Pienso
 */
public class Pienso {

    private Animal animal;
    private double pesoKG;
    private double calorias;
    private double precio;
    private double cantidadProteinas;
    private String tipoCarne;

    /**
     * Returnamos el objeto animal
     * Metodo get del atributo animal
     * @return
     */
    public Animal getAnimal() {
        return animal;
    }

    /**
     * Cambia el valor de animal con el valor adquirido por parametro
     * Metodo set del atributo animal
     * @param animal
     */
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    /**
     * Metodo get del atributo peso
     * Returna el valor de peso
     * @return
     */
    public double getPesoKG() {
        return pesoKG;
    }

    /**
     * Metodo set de peso
     * Cambia el valor de peso por el valor adquirido por parametro
     * @param pesoKG
     */
    public void setPesoKG(double pesoKG) {
        this.pesoKG = pesoKG;
    }

    /**
     * Constructor de la clase que inicializa los campos de animal y peso
     */
    public Pienso () {

        this.animal=new Animal();
        this.pesoKG=0;

    }

    /**
     * Metodo que returna el valor de calorias
     * Metodo get del atributo calorias
     * @return
     */
    public double getCalorias() {
        return calorias;
    }

    /**
     * Metodo set del atributo calorias
     * Cambia el valor de callorias por el valor adquirido por paramtro
     * @param calorias
     */
    public void setCalorias(double calorias) {
        this.calorias = calorias;
    }

    /**
     * Metodo get del valor precio
     * Devuelve el valor del atributo get
     * @return
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Metodo set del valor precio
     * Cambia el valor de precio por el valor adquirido por paramtro
     * @param precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Metodo get de cantidadProteinas
     * Devuelve el valor de cantidadProteinas
     * @return
     */
    public double getCantidadProteinas() {
        return cantidadProteinas;
    }

    /**
     * Metodo set de cantidadProteinas
     * Cambia el valor de cantidadProteinas por el valor adquirido por parametro
     * @param cantidadProteinas
     */
    public void setCantidadProteinas(double cantidadProteinas) {
        this.cantidadProteinas = cantidadProteinas;
    }

    /**
     * Metodo get de tipoCarne
     * Devuelve el valor de tipoCarne
     * @return
     */
    public String getTipoCarne() {
        return tipoCarne;
    }

    /**
     * Metodo set de tipo carne
     * Cambia el valor de tipo carne por el valor adquirido por parametro
     * @param tipoCarne
     */
    public void setTipoCarne(String tipoCarne) {
        this.tipoCarne = tipoCarne;
    }

    /**
     * Constructor de la clase Pienso de sobrecarga
     * @param animal
     * @param pesoKG
     * @param calorias
     * @param precio
     * @param cantidadProteinas
     * @param tipoCarne
     */
    public Pienso(Animal animal, double pesoKG, double calorias, double precio,double cantidadProteinas,String tipoCarne) {
        this.animal = animal;
        this.pesoKG = pesoKG;
        this.calorias=calorias;
        this.precio=precio;
        this.cantidadProteinas=cantidadProteinas;
        this.tipoCarne=tipoCarne;

    }

    /**
     * Metodo toString de la clase Pienso
     * Returna un String con el valor de todos los campos
     * @return
     */
    @Override
    public String toString() {
        return "Pienso{" +
                "animal=" + animal.toString() +
                ", pesoKG=" + pesoKG +
                ", calorias=" + calorias +
                ", precio=" + precio +
                ", cantidad proteinas = " + cantidadProteinas +
                ", tipo de carne = " + tipoCarne +
                '}';
    }
}
