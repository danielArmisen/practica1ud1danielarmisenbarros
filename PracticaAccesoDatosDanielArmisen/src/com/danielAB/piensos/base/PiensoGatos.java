package com.danielAB.piensos.base;

/**
 * Clase PiensoGatos que extiende de Pienso
 */
public class PiensoGatos extends Pienso{

    private boolean cantidadTaurina;
    private boolean leche;

    /**
     * Getters y Setters de la clase PiensoGatos
     * @return
     */
    public boolean getCantidadTaurina() {
        return cantidadTaurina;
    }

    public void setCantidadTaurina(boolean cantidadTaurina) {
        this.cantidadTaurina = cantidadTaurina;
    }

    public boolean getLeche() {
        return leche;
    }

    public void setLeche(boolean leche) {
        this.leche = leche;
    }

    public PiensoGatos() {
        this.cantidadTaurina = false;
        this.leche = false;
    }

    /**
     * Metodo toString de los atributos de la clase y de la clase padre
     * @return
     */
    @Override
    public String toString() {

        return super.toString() + "PiensoGatos{" +
                "cantidadTaurina=" + cantidadTaurina +
                "leche= " + leche +
                '}';
    }

    /**
     * Constructor con sobrecarga y con los atributos del padre
     * @param animal
     * @param pesoKG
     * @param calorias
     * @param precio
     * @param cantidadProteinas
     * @param tipoCarne
     * @param cantidadTaurina
     * @param leche
     */
    public PiensoGatos(Animal animal, int pesoKG, double calorias, double precio, double cantidadProteinas, String tipoCarne, boolean cantidadTaurina, boolean leche) {
        super(animal, pesoKG, calorias, precio, cantidadProteinas, tipoCarne);
        this.cantidadTaurina = cantidadTaurina;
        this.leche = leche;
    }
}
