package com.danielAB.piensos.base;

import java.time.LocalDate;

/**
 * Clase Animal
 */
public class Animal {

    private String animal;
    private String nombre;
    private int edad;
    private int tamaño;
    private String raza;
    private LocalDate fechaNacimiento;

    /**
     * Constructor que inicilaiza los atributos de Animal
     */
    public Animal() {

        this.animal="";
        this.nombre="";
        this.edad=0;
        this.tamaño=0;
        this.raza="";
        this.fechaNacimiento=null;

    }

    /**
     * Constructor de sobrecarga
     * @param animal
     * @param nombre
     * @param edad
     * @param tamaño
     * @param raza
     * @param fechaNacimiento
     */
    public Animal(String animal,String nombre,int edad,int tamaño,String raza, LocalDate fechaNacimiento) {

        this.animal=animal;
        this.nombre=nombre;
        this.edad=edad;
        this.tamaño=tamaño;
        this.raza=raza;
        this.fechaNacimiento = fechaNacimiento;

    }

    /**
     * Getters y Setters de los atributos de clase
     * @return
     */
    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.nombre = animal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Metodo toString de los atributos de la clase
     * @return
     */
    public String toString() {
        return "Animal{" +
                "animal=" + animal + '\'' +
                ", nombre='" + nombre +
                ", edad=" + edad +
                ", tamaño=" + tamaño +
                ", raza='" + raza + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }
}
