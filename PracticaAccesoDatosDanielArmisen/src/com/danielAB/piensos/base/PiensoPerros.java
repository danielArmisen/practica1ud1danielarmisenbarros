package com.danielAB.piensos.base;

/**
 * Clase PiensoPerros extiende de Pienso
 */
public class PiensoPerros extends Pienso{

    private boolean cantidadGrasas;
    private boolean cantidadCenizas;


    /**
     * Getters y Setters de los atributosl
     * @return
     */
    public boolean getCantidadGrasas() {
        return cantidadGrasas;
    }

    public void setCantidadGrasas(boolean cantidadGrasas) {
        this.cantidadGrasas = cantidadGrasas;
    }

    public boolean getCantidadCenizas() {
        return cantidadCenizas;
    }

    public void setCantidadCenizas(boolean cantidadCenizas) {
        this.cantidadCenizas = cantidadCenizas;
    }

    /**
     * Constructor de la clase que inicializa los atributos
     */
    public PiensoPerros() {
        this.cantidadGrasas = false;
        this.cantidadCenizas = false;
    }

    /**
     * Constructor de sobrecarga de la clase PiensoPerro con los campos del padre
     * @param animal
     * @param pesoKG
     * @param calorias
     * @param precio
     * @param cantidadProteinas
     * @param tipoCarne
     * @param cantidadGrasas
     * @param cantidadCenizas
     */
    public PiensoPerros(Animal animal, int pesoKG, double calorias, double precio, double cantidadProteinas, String tipoCarne, boolean cantidadGrasas, boolean cantidadCenizas) {
        super(animal, pesoKG, calorias, precio, cantidadProteinas, tipoCarne);
        this.cantidadGrasas = cantidadGrasas;
        this.cantidadCenizas = cantidadCenizas;
    }

    /**
     *Metodo ToString con los campos del  padre
     * @return
     */
    @Override
    public String toString() {

        return super.toString() + "PiensoPerros{" +
                "cantidadGrasas=" + cantidadGrasas +
                ", cantidadCenizas=" + cantidadCenizas +
                '}';
    }
}
