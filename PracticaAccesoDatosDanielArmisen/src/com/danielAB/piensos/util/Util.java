package com.danielAB.piensos.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.net.FileNameMap;

public class Util {
    /**
     * Metodo que recibe un String
     * Usamos el metodo de JOptionPane.showMessageDialog para sacar la pantalla de error en caso de haber ocurrido algo erroneo
     * A este le pasamos in string como titulo de la pantalla, y el mensaje de error
     * @param mError
     */
    public static void mensajeError (String mError) {

        JOptionPane.showMessageDialog(null, mError, "Ha ocurrido algo..." ,JOptionPane.ERROR_MESSAGE);

    }

    /**
     * Metodo que recibe dos Strings destinados al  mensaje de error y el titulo de la pantalla de error
     * Usamos la opcion showConfirmDialog de JOptionPane y le pasamos el mensaje recibido por parametro y el titulo recibido por parametro
     * Usaremos la opcion de si o no
     * @param mensaje
     * @param titulo
     * @return
     */
    public static int mensajeConfirmacion (String mensaje, String titulo) {

        return JOptionPane.showConfirmDialog(null,mensaje, titulo, JOptionPane.YES_NO_OPTION);

    }

    /**
     * Metodo que returna un FileChooser
     * Recibe un fichero, un String que sera el filtro de ficheros en la ventana de directorios del equipo y otro String con la extension del archivo
     * Si la ruta es distinta de null coge la ruta para usarla como predeterminada
     * Si la extension es distinta de null creamos un filtro de ficheros al que pasamos el tipo de fichero y la extension, y cambiamos el filtro del filechooser creado anteriormente
     * @param ruta
     * @param tipoArchivo
     * @param extension
     * @return
     */
    public static JFileChooser crearSelectorFichero (File ruta, String tipoArchivo, String extension) {

        JFileChooser selectorFichero = new JFileChooser();

        if (ruta!=null) {

            selectorFichero.setCurrentDirectory(ruta);

        }
        if (extension!=null){

            FileNameExtensionFilter extensionFiltro = new FileNameExtensionFilter(tipoArchivo, extension);
            selectorFichero.setFileFilter(extensionFiltro);

        }
        return selectorFichero;
    }


}
