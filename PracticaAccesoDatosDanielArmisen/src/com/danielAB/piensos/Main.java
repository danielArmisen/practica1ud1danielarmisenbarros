package com.danielAB.piensos;

import com.danielAB.piensos.gui.PiensosControlador;
import com.danielAB.piensos.gui.PiensosModelo;
import com.danielAB.piensos.gui.Vista;

import java.io.IOException;

/**
 * @author Daniel Armisen Barros
 * @version 10/11/2021
 */
public class Main {
    /**
     * Creamos Instancia de Vista
     * Creamos Instancia de piensoModelo
     * Creamos Instancia de piensoControlador y le pasamos por parametro las dos instancias anteriores
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Vista vista = new Vista();
        PiensosModelo piensosModelo = new PiensosModelo();
        PiensosControlador controlador = new PiensosControlador(vista,piensosModelo);

    }


}
