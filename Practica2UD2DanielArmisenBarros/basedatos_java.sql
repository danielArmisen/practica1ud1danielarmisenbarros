CREATE DATABASE if not exists piensos;
--
USE piensos;
--
create table if not exists duenios(
                                      idduenio int auto_increment primary key,
                                      nombre varchar(50) not null,
                                      apellidos varchar(150) not null,
                                      dni varchar(9) not null unique,
                                      fechanacimiento date,
                                      pais varchar(50));
--
create table if not exists animal (
                                      idanimal int auto_increment primary key,
                                      tipo varchar(30) not null,
                                      nombre varchar(100) not null,
                                      tamanio varchar(100) not null,
                                      edad date,
                                      raza varchar(100));
--
create table if not exists pienso(
                                     idpienso int auto_increment primary key,
                                     tipo varchar(50) not null,
                                     marca varchar(40) not null,
                                     nutrientes varchar(40) not null,
                                     idanimal int not null,
                                     idduenio int not null,
                                     precio float not null,
                                     fechacaducidad date);
--
alter table pienso
    add foreign key (idduenio) references duenios(idduenio),
    add foreign key (idanimal) references animal(idanimal);
--

create function existeDNI(f_dni varchar(9))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idduenio) from duenios)) do
            if  ((select dni from duenios where idduenio = (i + 1)) like f_dni) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;

create function piensoExiste(f_marca varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idpienso) from pienso)) do
            if  ((select marca from pienso where idpienso = (i + 1)) like f_marca) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;

create function animalNombreExiste(f_nombre varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idanimal) from animal)) do
            if  ((select nombre from animal where idanimal = (i + 1)) like f_nombre) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;

create function maximoRaza (f_raza varchar(50))
    returns int
begin
    if exists(select raza from animal where raza=f_raza) then
        return(select count(raza) from animal where raza = f_raza);
    else return 0;
    end if;
end;
create function minimoRaza (f_raza varchar (50))
    returns int
begin

    if exists(select raza from animal where raza=f_raza) then
        return (select count(raza) from animal where raza=f_raza);
    else return 0;

    end if;
end;

create function masDeUnaCompra (f_dni varchar(9))
    returns int
begin
    if exists (select dni from duenios where dni=f_dni) then
        return (select count(dni) from duenios where dni=f_dni);
    else return 0;
    end if;
end;


