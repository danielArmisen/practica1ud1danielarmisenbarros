package gui;

import javax.swing.*;
import java.awt.*;

public class Login extends JDialog{
     JPanel panel1;
     JButton bGuardar;
     JTextField txtIP;
     JTextField txtUser;
     JPasswordField txtContrasenia;
     JPasswordField txtContraseniaAdmin;

    private Frame frame;

    /**
     * Constructor d ela clase en la que inicializamos el frame y usamos el metodo componentes
     * @param f
     */
    public Login (Frame f) {
        super(f, "Opciones", true);
        this.frame = f;
        componentes();
    }

    /**
     * Metodo en el que configuramos la ventana Login
     */
    public void componentes (){

        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension (this.getWidth()+200, this.getHeight()) );
        this.setLocationRelativeTo(frame);

    }

}
