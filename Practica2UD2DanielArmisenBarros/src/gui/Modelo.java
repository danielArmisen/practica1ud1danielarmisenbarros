package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    /**
     * Declaracion de atributos de la clase y la creacion de GETTERS Y SETTERS de estos
     */
    private String ip;
private String user;
private String password;
private String adminpassword;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdminpassword() {
        return adminpassword;
    }

    public void setAdminpassword(String adminpassword) {
        this.adminpassword = adminpassword;
    }

    public Modelo() throws IOException {
        getPropValues();
    }

    private Connection conexion;

    /**
     * Metodo conectar en el que inicializamos la conexionn y ponemos la sentencia a ejecutar
     * Usamos el emtodo leer fichero y hacemos uso de split para sacar los campos que necesitamos para usarlos en la sentencia que se ejecutara
     * Ejecutamos la sentencia y cerramos conexion
     */
    public void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/piensos", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://"
                        + ip + ":3306/piensos", user, password);
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    /**
     * Leemos el fichero de basedatos.sql y creamos la base de datos
     * @return
     * @throws IOException
     */
    private String leerFichero () throws IOException {
        try (BufferedReader bf = new BufferedReader(new FileReader("basedatos.sql"))) {
            String linea = "";
            StringBuilder builder = new StringBuilder();
            while((linea =bf.readLine())!=null){
                builder.append(linea);
                builder.append(" ");
            }
            return builder.toString();
        }
    }

    /**
     * Cerramos la conexion
     */
    public void desconectar () {
        try{
            conexion.close();
            conexion = null;
        }catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Inicializaoms la conexion para poder ejecutar una sentencia como mysql
     * Hacemos uso de los parametros y para usarlos en los correspondientes interrogantes de la sentencia
     * y ejecutamos
     * @param tipo
     * @param nombre
     * @param tamanio
     * @param edad
     * @param raza
     * @throws SQLException
     */
    public void insertarAnimal (String tipo, String nombre, int tamanio, LocalDate edad, String raza) throws SQLException {
        String sentencia = "INSERT INTO animal (tipo,nombre,tamanio,edad,raza) "
                + "VALUES(?,?,?,?,?)";

        PreparedStatement st = null;
        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1, tipo);
            st.setString(2, nombre);
            st.setInt(3, tamanio);
            st.setString(4, String.valueOf(edad));
            st.setString(5, raza);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                st.close();
            }
        }
    }

    /**
     * Inicializaoms la conexion para poder ejecutar una sentencia como mysql
     * Hacemos uso de los parametros y para usarlos en los correspondientes interrogantes de la sentencia
     * y ejecutamos
     * @param nombre
     * @param apellido
     * @param DNI
     * @param fechaNac
     * @param pais
     * @throws SQLException
     */
    public void insertarDuenio (String nombre, String apellido, String DNI, LocalDate fechaNac, String pais) throws SQLException {
        String sentencia = "INSERT INTO duenios(nombre, apellidos, DNI, fechanacimiento, pais) "
                +  "VALUES (?,?,?,?,?)";
        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1, nombre);
            st.setString(2, apellido);
            st.setString(3, DNI);
            st.setString(4, String.valueOf(fechaNac));
            st.setString(5, pais);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }
    }

    /**
     * Inicializaoms la conexion para poder ejecutar una sentencia como mysql
     * Hacemos uso de los parametros y para usarlos en los correspondientes interrogantes de la sentencia
     * y ejecutamos
     * @param tipo
     * @param marca
     * @param nutrientes
     * @param animal
     * @param duenio
     * @param precio
     * @param fechaCac
     * @throws SQLException
     */
    public void insertarPienso (String tipo, String marca, String nutrientes, String animal, String duenio, double precio, LocalDate fechaCac) throws SQLException {
        String sentencia = "INSERT INTO pienso (tipo, marca, nutrientes, idanimal, idduenio, precio, fechacaducidad ) "
                + "VALUES(?,?,?,?,?,?,?)";

        PreparedStatement st = null;

        int idDuenio = Integer.valueOf(duenio.split(" ")[0]);
        int idAnimal = Integer.valueOf(animal.split(" ")[0]);

        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1,tipo);
            st.setString(2,marca);
            st.setString(3,nutrientes);
            st.setInt(4, idAnimal);
            st.setInt(5, idDuenio);
            st.setDouble(6, precio);
            st.setString(7, String.valueOf(fechaCac));
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * Metodo que modifica un animal a traves de una sentencia ejecutada para mysql
     * usando los parametros con los correspondiente interrogantes
     * @param tipo
     * @param nombre
     * @param tamanio
     * @param edad
     * @param raza
     * @param idAnimal
     * @throws SQLException
     */
    public void modificarAnimal (String tipo, String nombre, int tamanio, LocalDate edad, String raza, int idAnimal) throws SQLException {

        String sentencia = "UPDATE animal SET tipo=?, nombre=?, tamanio=?, edad=?, raza=? WHERE idAnimal=?";

        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1, tipo);
            st.setString(2, nombre);
            st.setInt(3, tamanio);
            st.setDate(4, Date.valueOf(edad));
            st.setString(5, raza);
            st.setInt(6, idAnimal);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * Metodo que modifica un animal a traves de una sentencia ejecutada para mysql
     * usando los parametros con los correspondiente interrogantes
     * @param nombre
     * @param apellido
     * @param DNI
     * @param fechaNac
     * @param pais
     * @param idDuenio
     * @throws SQLException
     */
    public void modificarDuenio (String nombre, String apellido, String DNI, LocalDate fechaNac, String pais, int idDuenio) throws SQLException {

        String sentencia = "UPDATE duenios SET nombre=?, apellidos=?, DNI=?, fechanacimiento=?, pais=? WHERE idDuenio=?";

        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1,nombre);
            st.setString(2, apellido);
            st.setString(3, DNI);
            st.setDate(4, Date.valueOf(fechaNac));
            st.setString(5, pais);
            st.setInt(6, idDuenio);
            st.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(st!=null) {
                st.close();
            }
        }

    }

    /**
     * Metodo que modifica un animal a traves de una sentencia ejecutada para mysql
     * usando los parametros con los correspondiente interrogantes
     * @param tipo
     * @param marca
     * @param nutrientes
     * @param animal
     * @param duenio
     * @param precio
     * @param fechaCac
     * @param idPienso
     * @throws SQLException
     */
    public void modificarPienso (String tipo, String marca, String nutrientes, String animal, String duenio, double precio, LocalDate fechaCac, int idPienso) throws SQLException {

        String sentencia = "UPDATE pienso SET tipo=?, marca=?, nutrientes=?, idanimal=?, idduenio=?, precio=?, fechacaducidad=? WHERE idPienso=?";

        PreparedStatement st = null;

        int idDuenio = Integer.valueOf(duenio.split(" ")[0]);
        int idAnimal = Integer.valueOf(animal.split(" ")[0]);

        try {
            st = conexion.prepareStatement(sentencia);
            st.setString(1,tipo);
            st.setString(2, marca);
            st.setString(3, nutrientes);
            st.setInt(4, idAnimal);
            st.setInt(5, idDuenio);
            st.setDouble(6, precio);
            st.setDate(7, Date.valueOf(fechaCac));
            st.setInt(8, idPienso);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * Elimminamos un animal a traves de la sentencia sql
     * @param idAnimal
     * @throws SQLException
     */
    public void borrarAnimal (int idAnimal) throws SQLException {

        String sentencia = "DELETE FROM animales WHERE idAnimal=?";

        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setInt(1, idAnimal);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * Eliminamos un dueño a traces de la sentencia sql
     * @param idDuenio
     * @throws SQLException
     */
    public void borrarDuenio (int idDuenio) throws SQLException {

        String sentencia = "DELETE FROM duenios WHERE idDuenio=?";

        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setInt(1, idDuenio);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * Eliminamos un pienso a traves de la sentencia sql
     * @param idPienso
     * @throws SQLException
     */
    public void borrarPienso (int idPienso) throws SQLException {

        String sentencia = "DELETE FROM pienso WHERE idPienso=?";

        PreparedStatement st = null;

        try {
            st = conexion.prepareStatement(sentencia);
            st.setInt(1, idPienso);
            st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st!=null) {
                st.close();
            }
        }

    }

    /**
     * A traves de la sentencia ejecutada hacemos una busqueda de los animales con los datos asignados las variables de la sentencia
     * @return
     * @throws SQLException
     */
    public ResultSet consultarAnimal () throws SQLException {

        String sentencia = "SELECT concat(idAnimal) AS 'ID', concat(tipo) AS 'Tipo animal', "
                + "concat(nombre) AS 'Nombre animal', concat(tamanio) AS 'Tamanio animal', "
                + "concat(edad) AS 'Edad animal', concat(raza) AS 'Raza animal' FROM animal";

        PreparedStatement st = null;
        ResultSet result = null;

        st = conexion.prepareStatement(sentencia);
        result = st.executeQuery();

        return result;
    }
    /**
     * A traves de la sentencia ejecutada hacemos una busqueda de los animales con los datos asignados las variables de la sentencia
     * @return
     * @throws SQLException
     */
    public ResultSet consultarDuenio () throws SQLException {

        String sentencia = "SELECT concat(idDuenio) AS 'ID', concat(nombre) AS 'Nombre duenio', "
                + "concat(apellidos) AS 'Apellidos duenio', concat(DNI) AS 'DNI duenio', "
                + "concat(fechanacimiento) AS 'Fecha de nacimiento', concat(pais) AS 'Pais duenio' FROM duenios";

        PreparedStatement st = null;
        ResultSet result = null;

        st = conexion.prepareStatement(sentencia);
        result = st.executeQuery();

        return result;
    }
    /**
     * A traves de la sentencia ejecutada hacemos una busqueda de los animales con los datos asignados las variables de la sentencia
     * @return
     * @throws SQLException
     */
    public ResultSet consultarPienso () throws SQLException {

        String sentencia = "SELECT concat(idPienso) AS 'ID', concat(pienso.tipo) AS 'Tipo piensos', "
                + "concat(pienso.marca) AS 'Marca pienso', concat(pienso.nutrientes) AS 'Nutrientes pienso', "
                + "concat(animal.idAnimal, ' - ', animal.nombre) AS 'Animal pienso', concat(duenios.idDuenio, ' - ', duenios.DNI) AS 'Duenio pienso', "
                + "concat(pienso.precio) AS 'Precio pienso', concat(pienso.fechacaducidad) AS 'Fecha de caducidad pienso' FROM pienso "
                + "INNER JOIN animal ON animal.idAnimal = pienso.idAnimal INNER JOIN duenios ON duenios.idDuenio = pienso.idDuenio";

        PreparedStatement st = null;
        ResultSet result = null;

        st = conexion.prepareStatement(sentencia);
        result = st.executeQuery();

        return result;
    }

    /**
     * Creamos una variable properties y usamos el fichero config.properties con nuestra configuracion de conexion
     * @throws IOException
     */
    public void getPropValues () throws IOException {
        InputStream input = null;
        try{
            Properties p = new Properties();
            String propFile = "config.properties";
            input = new FileInputStream(propFile);
            p.load(input);
            ip = p.getProperty("ip");
            user = p.getProperty("user");
            password = p.getProperty("pass");
            adminpassword = p.getProperty("admin");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (input!=null) {
                input.close();
            }
        }
    }

    /**
     * Con este metodo cambiamos los valores de nuestro fichero de conexion config.properties
     * @param ip
     * @param user
     * @param pass
     * @param adminPass
     */
    public void setPropValues (String ip, String user, String pass, String adminPass) {
        try{
            Properties p = new Properties();
            p.setProperty("ip", ip);
            p.setProperty("user", user);
            p.setProperty("pass", pass);
            p.setProperty("admin", adminPass);
            OutputStream output = new FileOutputStream("config.properties");
            p.store(output, null);
        }catch (IOException e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminpassword = adminPass;
    }

    /**
     * Con este metodo buscamos un dato que coincida con uno ya existente en la base de datos
     * @param nombre
     * @return
     */
    public boolean animalNombreExiste(String nombre) {
        boolean nombreExiste = false;
        String consulta = "SELECT animalNombreExiste(?)";
        PreparedStatement funcion;

        try {
            funcion = conexion.prepareCall(consulta);
            funcion.setString(1,nombre);
            ResultSet rs = funcion.executeQuery();
            rs.next();
            nombreExiste = rs.getBoolean(1);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return nombreExiste;
    }
    /**
     * Con este metodo buscamos un dato que coincida con uno ya existente en la base de datos
     * @param DNI
     * @return
     */
    public boolean duenioDNIYaExiste (String DNI) {
        boolean DNIexiste = false;
        String consulta = "SELECT existeDNI(?)";
        PreparedStatement funcion;

        try {
            funcion = conexion.prepareCall(consulta);
            funcion.setString(1,DNI);
            ResultSet rs = funcion.executeQuery();
            rs.next();
            DNIexiste = rs.getBoolean(1);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return DNIexiste;
    }
    /**
     * Con este metodo buscamos un dato que coincida con uno ya existente en la base de datos
     * @param marca
     * @return
     */

    public boolean piensoExiste (String marca) {
        boolean marcaExiste = false;
        String consulta = "SELECT piensoExiste(?)";
        PreparedStatement funcion;

        try {
            funcion = conexion.prepareCall(consulta);
            funcion.setString(1,marca);
            ResultSet rs = funcion.executeQuery();
            rs.next();
            marcaExiste = rs.getBoolean(1);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return marcaExiste;
    }





















































}
