package gui;

import base.enums.NutrientesPienso;
import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    /**
     * Componentes de la interfaz
     */

    JTabbedPane tabbedPane1;
     JPanel panel1;
     JButton bAniadirDuenio;
     JButton bModificarDuenio;
     JButton bEliminarDuenio;
     JTextField txtNombreDuenio;
     JTextField txtApellidoDuenio;
     JTextField txtDNIDuenio;
     JTextField txtPaisDuenio;
     JTable tableDuenio;
     JTextField txtNombreAnimal;
     JSlider sliderTamanioAnimal;
     JComboBox razaComboAnimal;
     JButton bAniadirAnimal;
     JButton bModificarAnimal;
     JButton bEliminarAnimal;
     JTable tablaAnimal;
     JButton bAniadirPienso;
     JButton bModificarPienso;
     JButton bEliminarPienso;
     JTextField txtMarcaPienso;
     JComboBox nutrientesComboPienso;
     JComboBox animalComboPienso;
     JComboBox duenioComboPienso;
     JTable tablaPienso;
    DateTimePicker edadAnimal;
     JTextPane txtPrecioPienso;
    JScrollPane scrollPane;
     JRadioButton radioBPerro;
     JRadioButton radioBGato;
     JRadioButton grandeRadioButton;
     JRadioButton medianoRadioButton;
     JRadioButton pequenioRadioButton;
     DatePicker datepickerAnimal;
     DatePicker datePickerPienso;
     DatePicker datePickerDuenio;
     JTextArea precioPienso;

    JLabel estado;

    DefaultTableModel dtmDuenio;
    DefaultTableModel dtmAnimal;
    DefaultTableModel dtmPienso;

    JMenuItem opciones;
    JMenuItem desconectar;
    JMenuItem salir;

    Login login;
    JDialog adminPasswordDialog;
    JButton bValidar;
    JPasswordField contraseniaAdmin;

    /**
     * Constructor de la Clase vista, le ponemos titulo y llamamos al metodo componentes
     */
    public Vista() {

       super("Practica2UD2");
       componentes();

    }

    /**
     * Metodo setEnumComboBox, rellena de datos el combobox. Y te posiciona en el dato -1 que es la seleccion de nada
     */
    private void setEnumComboBox() {

        for (NutrientesPienso n: NutrientesPienso.values()) {
            nutrientesComboPienso.addItem(n.getNombrePienso());
        }
        nutrientesComboPienso.setSelectedIndex(-1);
    }

    /**
     *Metodo componentes, donde configuramos la interfaz, le damos tamaño y la hacemos visible
     * Iniciamos el componente de Login que es otra vista y usamos los diferentes metodos
     * setMenu()
     * setAdminLogin()
     * setEnumcombobox()
     * setTableModel()
     */

    private void componentes() {

        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()));
        this.setLocationRelativeTo(this);

        login = new Login(this);
        setMenu();
        setAdminLogin();
        setEnumComboBox();
        setTableModel();

    }

    /**
     * Metodo setTableModel, este metodo inicializa
     * las defaultTableModels de cada pestaña a la cual le asignaremos la tabla correspondiente. Para poder visualizar los datos de esa tabla en el DefaulttableModel
     */
    private void setTableModel() {

        this.dtmDuenio = new DefaultTableModel();
        this.tableDuenio.setModel(dtmDuenio);
        this.dtmAnimal = new DefaultTableModel();
        this.tablaAnimal.setModel(dtmAnimal);
        this.dtmPienso = new DefaultTableModel();
        this.tablaPienso.setModel(dtmPienso);

    }

    /**
     * Metodo setMenu, es un metodo que inicializa un menu y su desplegable.
     *
     */
    private void setMenu () {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        opciones = new JMenuItem("Opciones");
        opciones.setActionCommand("Opciones");
        desconectar = new JMenuItem("Desconectar");
        desconectar.setActionCommand("Desconectar");
        salir = new JMenuItem("Salir");
        salir.setActionCommand("Salir");

        menu.add(opciones);
        menu.add(desconectar);
        menu.add(salir);
        menuBar.add(menu);
        menuBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(menuBar);

    }

    /**
     * Metodo abre la ventana con las opciones de conexion de la base de datos
     */
    private void setAdminLogin() {

        bValidar = new JButton("Validar");
        bValidar.setActionCommand("opciones");
        contraseniaAdmin = new JPasswordField();
        contraseniaAdmin.setPreferredSize(new Dimension(100,26));
        Object[] opciones = new Object[] {contraseniaAdmin,bValidar};
        JOptionPane jop = new JOptionPane("Introduce la conexion", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, opciones);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }


}
