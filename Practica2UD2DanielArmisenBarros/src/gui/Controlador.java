package gui;

import base.enums.RazasGato;
import base.enums.RazasPerro;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, le pasamos por parametros la vista y el modelo
     * inicializamos el modelo y la vista, conectamos con la base de datos y usamos los metodos para que os botones, la ventana y los items de la vista esten a la escucha
     * @param vista
     * @param modelo
     */
    public Controlador (Vista vista, Modelo modelo) {

        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        addActionListeners(this);
        addItemListeners(this);
        addWindowsListener(this);
        refreshAll();
    }

    /**
     * Metodo al que le pasamos el controlador como parametro
     * @param controlador
     */
    private void addItemListeners(Controlador controlador) {}

    /**
     * Metodo que refresca las 3 ventanas de la interfaz
     */
    private void refreshAll () {

        refrescarDuenio();
        refrescarAnimal();
        refrescarPienso();
        refrescar = false;
    }

    /**
     * Metodo que asigna un listener a los componentes que necesiten un click para activar su funcion
     * @param listener
     */
    private void addActionListeners (ActionListener listener) {
        vista.tableDuenio.getSelectionModel().addListSelectionListener(this);
        vista.tablaPienso.getSelectionModel().addListSelectionListener(this);
        vista.tablaAnimal.getSelectionModel().addListSelectionListener(this);
        vista.radioBPerro.addActionListener(listener);
        vista.radioBGato.addActionListener(listener);
        vista.bAniadirDuenio.addActionListener(listener);
        vista.bAniadirDuenio.setActionCommand("aniadirDuenio");
        vista.bAniadirAnimal.addActionListener(listener);
        vista.bAniadirAnimal.setActionCommand("aniadirAnimal");
        vista.bAniadirPienso.addActionListener(listener);
        vista.bAniadirPienso.setActionCommand("aniadirPienso");
        vista.bEliminarDuenio.addActionListener(listener);
        vista.bEliminarDuenio.setActionCommand("eliminarDuenio");
        vista.bEliminarAnimal.addActionListener(listener);
        vista.bEliminarAnimal.setActionCommand("eliminarAnimal");
        vista.bEliminarPienso.addActionListener(listener);
        vista.bEliminarPienso.setActionCommand("eliminarPienso");
        vista.bModificarDuenio.addActionListener(listener);
        vista.bModificarDuenio.setActionCommand("modificarDuenio");
        vista.bModificarAnimal.addActionListener(listener);
        vista.bModificarAnimal.setActionCommand("modificarAnimal");
        vista.bModificarPienso.addActionListener(listener);
        vista.bModificarPienso.setActionCommand("modificarPienso");
        vista.login.bGuardar.addActionListener(listener);
        vista.opciones.addActionListener(listener);
        vista.desconectar.addActionListener(listener);
        vista.salir.addActionListener(listener);
        vista.bValidar.addActionListener(listener);
    }

    /**
     * Asignamos un listener a la ventana
     * @param listener
     */
    private void addWindowsListener (WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Metodo que sirve para cambiar los campos de la ventana cuando clickemos en un item de los DefaultTableModels
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablaAnimal.getSelectionModel())) {
                int row = vista.tablaAnimal.getSelectedRow();
                if (vista.tablaAnimal.getValueAt(row,1).equals("Gato")) {
                    vista.radioBGato.doClick();
                }else {
                    vista.radioBPerro.doClick();
                }
                vista.txtNombreAnimal.setText(String.valueOf(vista.tablaAnimal.getValueAt(row,2)));
                vista.sliderTamanioAnimal.setValue(Integer.parseInt(String.valueOf(vista.tablaAnimal.getValueAt(row,3))));
                vista.datepickerAnimal.setDate(Date.valueOf(String.valueOf(vista.tablaAnimal.getValueAt(row,4))).toLocalDate());
                vista.razaComboAnimal.setSelectedItem(String.valueOf(vista.tablaAnimal.getValueAt(row, 5)));
            }else if (e.getSource().equals(vista.tableDuenio.getSelectionModel())) {
                int row = vista.tableDuenio.getSelectedRow();
                vista.txtNombreDuenio.setText(String.valueOf(vista.tableDuenio.getValueAt(row,1)));
                vista.txtApellidoDuenio.setText(String.valueOf(vista.tableDuenio.getValueAt(row,2)));
                vista.txtDNIDuenio.setText(String.valueOf(vista.tableDuenio.getValueAt(row,3)));
                vista.datePickerDuenio.setDate((LocalDate.parse(String.valueOf(vista.tableDuenio.getValueAt(row,4)))));
                vista.txtPaisDuenio.setText(String.valueOf(vista.tableDuenio.getValueAt(row,5)));
                System.out.println(row);
            }else if (e.getSource().equals(vista.tablaPienso.getSelectionModel())) {
                int row = vista.tablaPienso.getSelectedRow();
                if (vista.tablaPienso.getValueAt(row,1).equals("Grande")) {
                    vista.grandeRadioButton.doClick();
                }else if (vista.tablaPienso.getValueAt(row,1).equals("Mediano")) {
                    vista.medianoRadioButton.doClick();
                }else {
                    vista.pequenioRadioButton.doClick();
                }
                vista.txtMarcaPienso.setText(String.valueOf(vista.tablaPienso.getValueAt(row,2)));
                vista.nutrientesComboPienso.setSelectedItem(String.valueOf(vista.tablaPienso.getValueAt(row,3)));
                vista.animalComboPienso.setSelectedItem(String.valueOf(vista.tablaPienso.getValueAt(row,4)));
                vista.duenioComboPienso.setSelectedItem(String.valueOf(vista.tablaPienso.getValueAt(row,5)));
                vista.precioPienso.setText(String.valueOf(vista.tablaPienso.getValueAt(row,6)));
                vista.datePickerPienso.setDate(LocalDate.parse(String.valueOf(vista.tablaPienso.getValueAt(row,7))));
            }else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tablaAnimal.getSelectionModel())) {
                    borrarCamposAnimal();
                } else if (e.getSource().equals(vista.tablaPienso.getSelectionModel())) {
                    borrarCamposPienso();
                } else if (e.getSource().equals(vista.tableDuenio.getSelectionModel())) {
                    borrarCamposDuenios();
                }
            }
        }

    }

    /**
     * Metodo que usa el action Command de los distintos componentes de la interfaz
     * Es un metodo que usa un case para distinguir entre los diferentes componentes usados de la interfaz
     * Cuando hagamos uso de un componente hara la funcion asignada por el case correspondiente
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Gato":
                    vista.razaComboAnimal.removeAllItems();
                    for (RazasGato re : RazasGato.values()) {
                        vista.razaComboAnimal.addItem(re.getRaza());
                    }
                    vista.razaComboAnimal.setSelectedIndex(-1);
                    if (vista.radioBPerro.isSelected()) {
                        vista.razaComboAnimal.removeAllItems();

                    }
                break;
            case "Perro":
                vista.razaComboAnimal.removeAllItems();
                    for (RazasPerro r : RazasPerro.values()) {
                        vista.razaComboAnimal.addItem(r.getRaza());
                    }
                    vista.razaComboAnimal.setSelectedIndex(-1);
                    if (vista.radioBGato.isSelected()) {
                        vista.razaComboAnimal.removeAllItems();

                    }
                break;
            case "Opciones":
                vista.login.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                vista.desconectar.setText("Conectar");
                vista.desconectar.setActionCommand("Conectar");
                break;
            case "Conectar":
                modelo.conectar();
                vista.desconectar.setText("Desconectar");
                vista.desconectar.setActionCommand("Desconectar");
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.contraseniaAdmin.getPassword()).equals(modelo.getAdminpassword())) {
                    vista.contraseniaAdmin.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.login.setVisible(true);
                }else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "Guardar":
                modelo.setPropValues(String.valueOf(vista.login.txtIP.getText()),
                vista.login.txtUser.getText(),
                String.valueOf(vista.login.txtContrasenia.getPassword()),
                String.valueOf(vista.login.txtContraseniaAdmin.getPassword()));
                vista.login.dispose();
                vista.dispose();
                try {
                    new Controlador(new Vista(), new Modelo());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case "aniadirAnimal":
                try {
                    if (comprobarAnimalVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaAnimal.clearSelection();
                    }else if (modelo.animalNombreExiste(vista.txtNombreAnimal.getText())) {
                        Util.showInfoAlert("Ya existe el Animal introducido, introdudca un DNI distinto");
                        vista.tableDuenio.clearSelection();
                    }else {
                        if (vista.radioBPerro.isSelected()) {
                            modelo.insertarAnimal(
                                    "Perro",
                                    vista.txtNombreAnimal.getText(),
                                    vista.sliderTamanioAnimal.getValue(),
                                    vista.datepickerAnimal.getDate(),
                                    String.valueOf(vista.razaComboAnimal.getSelectedItem())
                            );
                        }else if (vista.radioBGato.isSelected()) {
                            modelo.insertarAnimal(
                                    "Gato",
                                    vista.txtNombreAnimal.getText(),
                                    vista.sliderTamanioAnimal.getValue(),
                                    vista.datepickerAnimal.getDate(),
                                    String.valueOf(vista.razaComboAnimal.getSelectedItem())
                            );
                        }

                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce los numeros en los campos numericos");
                    vista.tablaAnimal.clearSelection();
                }
                borrarCamposAnimal();
                refrescarAnimal();
                break;
            case "modificarAnimal":
                try {
                    if (comprobarAnimalVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaAnimal.clearSelection();
                    }else {
                        if (vista.radioBPerro.isSelected()) {
                            modelo.modificarAnimal(
                                    "Perro",
                                    vista.txtNombreAnimal.getText(),
                                    vista.sliderTamanioAnimal.getValue(),
                                    vista.datepickerAnimal.getDate(),
                                    String.valueOf(vista.razaComboAnimal.getSelectedItem()),
                                    Integer.parseInt((String) vista.tablaAnimal.getValueAt(vista.tablaAnimal.getSelectedRow(),0))
                            );
                        }else if (vista.radioBGato.isSelected()) {
                            modelo.modificarAnimal(
                                    "Gato",
                                    vista.txtNombreAnimal.getText(),
                                    vista.sliderTamanioAnimal.getValue(),
                                    vista.datepickerAnimal.getDate(),
                                    String.valueOf(vista.razaComboAnimal.getSelectedItem()),
                                    Integer.parseInt((String) vista.tablaAnimal.getValueAt(vista.tablaAnimal.getSelectedRow(),0))
                            );
                        }

                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce numeros en los campos numericos");
                    vista.tablaAnimal.clearSelection();
                }
                borrarCamposAnimal();
                refrescarAnimal();
                break;
            case "eliminarAnimal":
                try {
                    modelo.borrarAnimal(Integer.parseInt((String) vista.tablaAnimal.getValueAt(vista.tablaAnimal.getSelectedRow(),0)));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    Util.showErrorAlert("No puedes borrar este Animal, porque esta usandose en otra tabla");
                }
                borrarCamposAnimal();
                refrescarAnimal();
                break;
            case "aniadirDuenio":
                try {
                    if (comprobarDuenioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableDuenio.clearSelection();
                    }else if (modelo.duenioDNIYaExiste(vista.txtDNIDuenio.getText())) {
                       Util.showInfoAlert("Ya existe el Dueño introducido, introdudca un DNI distinto");
                        vista.tableDuenio.clearSelection();
                    }else {
                        modelo.insertarDuenio(vista.txtNombreDuenio.getText(),
                                vista.txtApellidoDuenio.getText(),
                                vista.txtDNIDuenio.getText(),
                                vista.datePickerDuenio.getDate(),
                                vista.txtPaisDuenio.getText());
                        refrescarDuenio();
                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce numeros en los campos numericos");
                    vista.tableDuenio.clearSelection();
                }
                borrarCamposDuenios();
                refrescarDuenio();
                break;
            case "modificarDuenio":
                try {
                    if (comprobarDuenioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableDuenio.clearSelection();
                    }else if (modelo.duenioDNIYaExiste(vista.txtDNIDuenio.getText())) {
                        modelo.modificarDuenio(vista.txtNombreDuenio.getText(),
                                vista.txtApellidoDuenio.getText(),
                                vista.txtDNIDuenio.getText(),
                                vista.datePickerDuenio.getDate(),
                                vista.txtPaisDuenio.getText(),
                                Integer.parseInt((String) vista.tableDuenio.getValueAt(vista.tableDuenio.getSelectedRow(),0)));
                        refrescarDuenio();
                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce numeros en los campos numericos");
                    vista.tableDuenio.clearSelection();
                }
                borrarCamposDuenios();
                refrescarDuenio();
                break;
            case "eliminarDuenio":
                try {
                    modelo.borrarDuenio(Integer.parseInt((String) vista.tableDuenio.getValueAt(vista.tableDuenio.getSelectedRow(),0)));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    Util.showErrorAlert("No puedes borrar este Dueño, porque esta usandose en otra tabla");
                }
                borrarCamposDuenios();
                refrescarDuenio();
                break;
            case "aniadirPienso":
                try {
                    if (comprobarPiensoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaPienso.clearSelection();
                    }else if (modelo.piensoExiste(vista.txtMarcaPienso.getText())) {
                        Util.showInfoAlert("Ya existe el Pienso introducido, introdudca un DNI distinto");
                        vista.tableDuenio.clearSelection();
                    }else {
                            if (vista.pequenioRadioButton.isSelected()) {
                                modelo.insertarPienso(
                                        "Pequeño",
                                        vista.txtMarcaPienso.getText(),
                                        String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                        String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                        String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                        Double.parseDouble(vista.precioPienso.getText()),
                                        vista.datePickerPienso.getDate()
                                );
                            } else if (vista.medianoRadioButton.isSelected()) {
                                modelo.insertarPienso(
                                        "Mediano",
                                        vista.txtMarcaPienso.getText(),
                                        String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                        String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                        String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                        Double.parseDouble(vista.precioPienso.getText()),
                                        vista.datePickerPienso.getDate()
                                );
                            }else if (vista.grandeRadioButton.isSelected()) {
                                modelo.insertarPienso(
                                        "Grande",
                                        vista.txtMarcaPienso.getText(),
                                        String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                        String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                        String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                        Double.parseDouble(vista.precioPienso.getText()),
                                        vista.datePickerPienso.getDate()
                                );
                            }
                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce numeros en los campos numericos");
                    vista.tablaPienso.clearSelection();
                }
                borrarCamposPienso();
                refrescarPienso();
                break;
            case "modificarPienso":
                try {
                    if (comprobarPiensoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaPienso.clearSelection();
                    }else {
                        if (vista.pequenioRadioButton.isSelected()) {
                            modelo.modificarPienso(
                                    "Pequeño",
                                    vista.txtMarcaPienso.getText(),
                                    String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                    String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                    String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                    Double.parseDouble(vista.precioPienso.getText()),
                                    vista.datePickerPienso.getDate(),
                                    Integer.parseInt((String) vista.tablaPienso.getValueAt(vista.tablaPienso.getSelectedRow(),0))
                            );
                        } else if (vista.medianoRadioButton.isSelected()) {
                            modelo.modificarPienso(
                                    "Mediano",
                                    vista.txtMarcaPienso.getText(),
                                    String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                    String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                    String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                    Double.parseDouble(vista.precioPienso.getText()),
                                    vista.datePickerPienso.getDate(),
                                    Integer.parseInt((String) vista.tablaPienso.getValueAt(vista.tablaPienso.getSelectedRow(),0))
                            );
                        }else if (vista.grandeRadioButton.isSelected()) {
                            modelo.modificarPienso(
                                    "Grande",
                                    vista.txtMarcaPienso.getText(),
                                    String.valueOf(vista.nutrientesComboPienso.getSelectedItem()),
                                    String.valueOf(vista.animalComboPienso.getSelectedItem()),
                                    String.valueOf(vista.duenioComboPienso.getSelectedItem()),
                                    Double.parseDouble(vista.precioPienso.getText()),
                                    vista.datePickerPienso.getDate(),
                                    Integer.parseInt((String) vista.tablaPienso.getValueAt(vista.tablaPienso.getSelectedRow(),0))
                            );
                        }
                    }
                }catch (NumberFormatException | SQLException e1) {
                    Util.showErrorAlert("Introduce numeros en los campos numericos");
                    vista.tablaPienso.clearSelection();
                }
                borrarCamposPienso();
                refrescarPienso();
                break;
            case "eliminarPienso":
                try {
                    modelo.borrarPienso(Integer.parseInt((String) vista.tablaPienso.getValueAt(vista.tablaPienso.getSelectedRow(),0)));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                borrarCamposPienso();
                refrescarPienso();
                break;
        }
    }

    /**
     * Metodo refrescar animal, este metodo añade el campo añadido a la base de datos a la DefaultTableModel correspondiente a la pestaña que estemos usando
     * Ademas introducimos dicho campo en la comboBox de Animal, solo con un campo para poder usarlo como campo de este tabla
     */
    private void refrescarAnimal () {

        try {
            vista.tablaAnimal.setModel(construirTablaAnimal(modelo.consultarAnimal()));
            vista.animalComboPienso.removeAllItems();
            for (int i=0;i<vista.dtmAnimal.getRowCount();i++) {
                vista.animalComboPienso.addItem(vista.dtmAnimal.getValueAt(i,0)+" - "+
                vista.dtmAnimal.getValueAt(i,2));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo construir tabla, este metodo sirve para crear las columnas de la DefaultTableModel, mientras haya un campo en la tabla, esta la añadira como columna en el DefaultTableModel
     * Ademas le da valores a los campos
     * @param result
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaAnimal (ResultSet result) throws SQLException {

        ResultSetMetaData metaData =  result.getMetaData();
        Vector<String> columnaNombres = new Vector<>();
        int columnasNum = metaData.getColumnCount();
        for (int i = 1;i<=columnasNum;i++) {
            columnaNombres.add(metaData.getColumnName(i));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(result,columnasNum,data);
        vista.dtmAnimal.setDataVector(data,columnaNombres);
        return vista.dtmAnimal;
    }

    /**
     * Metodo refrescar animal, este metodo añade el campo añadido a la base de datos a la DefaultTableModel correspondiente a la pestaña que estemos usando
     * Ademas introducimos dicho campo en la comboBox de Dueño, solo con un campo para poder usarlo como campo de este tabla
     */
    private void refrescarDuenio(){

        try {
            vista.tableDuenio.setModel(construirTablaDuenio(modelo.consultarDuenio()));
            vista.duenioComboPienso.removeAllItems();
            for (int i=0;i<vista.dtmDuenio.getRowCount();i++) {
                vista.duenioComboPienso.addItem(vista.dtmDuenio.getValueAt(i,0)+" - "+
                vista.dtmDuenio.getValueAt(i,3));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }

    }
    /**
     * Metodo construir tabla, este metodo sirve para crear las columnas de la DefaultTableModel, mientras haya un campo en la tabla, esta la añadira como columna en el DefaultTableModel
     * Ademas le da valores a los campos
     * @param result
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaDuenio (ResultSet result) throws SQLException {

        ResultSetMetaData metaData = result.getMetaData();
        Vector<String> columnaNombres = new Vector<>();
        int columnaNum = metaData.getColumnCount();
        for (int i=1;i<=columnaNum;i++) {
            columnaNombres.add(metaData.getColumnName(i));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(result,columnaNum,data);
        vista.dtmDuenio.setDataVector(data,columnaNombres);
        return vista.dtmDuenio;
    }
    /**
     * Metodo refrescar animal, este metodo añade el campo añadido a la base de datos a la DefaultTableModel correspondiente a la pestaña que estemos usando
     * Ademas introducimos dicho campo en la comboBox de Pienso, solo con un campo para poder usarlo como campo de este tabla
     */
    private void refrescarPienso () {

        try {
            vista.tablaPienso.setModel(construirTablePienso(modelo.consultarPienso()));
        }catch (SQLException e1) {
            e1.printStackTrace();
        }

    }
    /**
     * Metodo construir tabla, este metodo sirve para crear las columnas de la DefaultTableModel, mientras haya un campo en la tabla, esta la añadira como columna en el DefaultTableModel
     * Ademas le da valores a los campos
     * @param result
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablePienso (ResultSet result) throws SQLException {

        ResultSetMetaData metaData = result.getMetaData();
        Vector<String> columnaNombres = new Vector<>();
        int columnaNum = metaData.getColumnCount();
        for (int i=1;i<=columnaNum;i++) {
            columnaNombres.add(metaData.getColumnName(i));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(result,columnaNum,data);
        vista.dtmPienso.setDataVector(data,columnaNombres);
        return vista.dtmPienso;
    }

    /**
     * Este metodo consiste en mientras lea que hay datos en los campos asigna al nuemero del campo correspondiente un valor
     * @param result
     * @param columnaNum
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet result, int columnaNum, Vector<Vector<Object>> data) throws SQLException {
        while (result.next()) {
            Vector<Object> vector = new Vector<>();
            for (int i = 1; i <= columnaNum; i++) {
                vector.add(result.getObject(i));
            }
            data.add(vector);
        }
    }

    /**
     * Cambia los valores de cada campo a 0
     */
    private void borrarCamposAnimal() {

        vista.txtNombreAnimal.setText("");
        vista.sliderTamanioAnimal.setValue(0);
        vista.datepickerAnimal.setText("");
        vista.razaComboAnimal.setSelectedIndex(-1);

    }
    /**
     * Cambia los valores de cada campo a 0
     */
    private void borrarCamposDuenios () {

        vista.txtNombreDuenio.setText("");
        vista.txtApellidoDuenio.setText("");
        vista.txtDNIDuenio.setText("");
        vista.datePickerDuenio.setText("");
        vista.txtPaisDuenio.setText("");

    }
    /**
     * Cambia los valores de cada campo a 0
     */
    private void borrarCamposPienso () {

        vista.txtMarcaPienso.setText("");
        vista.nutrientesComboPienso.setSelectedIndex(-1);
        vista.animalComboPienso.setSelectedIndex(-1);
        vista.duenioComboPienso.setSelectedIndex(-1);
        vista.precioPienso.setText("");
        vista.datePickerPienso.setText("");

    }

    /**
     * Comprueba que los campos de una pestaña estan vacios
     * @return
     */
    private boolean comprobarAnimalVacio () {

        return (!vista.radioBGato.isSelected() &&
                !vista.radioBPerro.isSelected()) ||
                vista.txtNombreAnimal.getText().isEmpty() ||
                vista.datepickerAnimal.getText().isEmpty() ||
                vista.razaComboAnimal.getSelectedIndex() == -1;

    }
    /**
     * Comprueba que los campos de una pestaña estan vacios
     * @return
     */
    private boolean comprobarDuenioVacio () {

        return vista.txtNombreDuenio.getText().isEmpty() ||
                vista.txtApellidoDuenio.getText().isEmpty() ||
                vista.txtDNIDuenio.getText().isEmpty() ||
                vista.datePickerDuenio.getText().isEmpty() ||
                vista.txtPaisDuenio.getText().isEmpty();

    }
    /**
     * Comprueba que los campos de una pestaña estan vacios
     * @return
     */
    private boolean comprobarPiensoVacio () {

        return (!vista.pequenioRadioButton.isSelected() &&
                !vista.medianoRadioButton.isSelected() &&
                !vista.grandeRadioButton.isSelected()) ||
                vista.txtMarcaPienso.getText().isEmpty() ||
                vista.nutrientesComboPienso.getSelectedIndex() == -1 ||
                vista.animalComboPienso.getSelectedIndex() == -1 ||
                vista.duenioComboPienso.getSelectedIndex() == -1 ||
                vista.precioPienso.getText().isEmpty() ||
                vista.datePickerPienso.getText().isEmpty();

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
