package util;

import javax.swing.*;

public class Util {
    /**
     * Usamos JOptionPane para en caso de error, que aparezca una pestaña con la informacion que necesitemos aportar al usuario que este usando nuestra aplicacion
     * @param mensaje
     */
    public static void showErrorAlert(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Usamos JOptionPane para en caso de error, que aparezca una pestaña con la informacion que necesitemos aportar al usuario que este usando nuestra aplicacion
     * @param mensaje
     */
    public static void showWarningAlert (String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Aviso", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Usamos JOptionPane para en caso de error, que aparezca una pestaña con la informacion que necesitemos aportar al usuario que este usando nuestra aplicacion
     * @param mensaje
     */
    public static void showInfoAlert (String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Informacion", JOptionPane.INFORMATION_MESSAGE);
    }

}
