package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

import java.io.IOException;

public class Principal {
    /**
     * Inicializamos una instancia de vista, otra de Modelo y otra de Controlador, y le pasamos a la instancia de Controlador las instancias de vista y modelo por parametros
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }

}
