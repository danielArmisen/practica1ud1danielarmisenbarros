package base.enums;

public enum NutrientesPienso {
    /**
     * ENUM con distintos tipos de nutrientes de piensos
     * GETTERS Y SETTERS
     */
    HIDRATOSCARBONO("Hidratos de Carbono"),
    LIPIDOSSATURADOS("Lipidos Saturados"),
    LIPIDOSINSATURADOS("Lipidos Insaturados"),
    PROTEINAS("Proteinas"),
    VITAMINAS("Vitaminas"),
    MINERALES("Minerales");

    private String nombrePienso;

    NutrientesPienso(String nombrePienso) {

        this.nombrePienso=nombrePienso;

    }

    public String getNombrePienso() {
        return nombrePienso;
    }
}
