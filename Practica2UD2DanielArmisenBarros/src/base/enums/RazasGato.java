package base.enums;

public enum RazasGato {
    /**
     * ENUM con distintas razas de Gato
     * GETTERS Y SETTERS
     */
    GATOBENGALA("Gato de Bengala"),
    HIMALAYO("Gato Himalayo"),
    GATOMANX("Gato Manx"),
    GATONEBELUNG("Gato Nebelung"),
    MAINECOON("Gato Maine Coon"),
    GATOCEILAN("Gato Ceilan"),
    GATOKURILIAN("Gato Kurilian"),
    GATORAGDOLL("Gato Ragdoll"),
    GATOTHAI("Gato Thai"),
    GATOGERMANREX("Gato German Rex"),
    GATODONSPHYNX("Gato Don Sphynx"),
    GATONEVA("Gato neva"),
    VANTURCO("Gato Van Turco"),
    GATOSNOWSHOE("Gato Snow Shoe"),
    GATOHANTILLYTIFFANY("Gato Hantilly-Tiffany"),
    GATOCOLORPOINT("Gato Color Point"),
    GATOANGORA("Gato Angora"),
    GATOMINSKIN("Gato Minskin"),
    GATOHABANA("Gato Habana"),
    GATOCOMUNEUROPEO("Gato Comun Europeo"),
    RAGAMUFFIN("Gato Raga Muffin"),
    ASHERA("Gato Ashera"),
    GATOBRITANICO("Gato Britanico"),
    GATOKHAO("Gato Khao"),
    GATOCARACAT("Gato Caracat"),
    GATOSINGAPURA("Gato Singapura");


    private String raza;

    RazasGato (String raza) {

        this.raza = raza;

    }

    public String getRaza() {
        return raza;
    }
}
