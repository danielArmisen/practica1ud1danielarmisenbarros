package base.enums;

public enum RazasPerro {
    /**
     * ENUM con distintas razas de Perro
     * GETTERS Y SETTERS
     */
    GORDON("Gordon"),
    PERROPOIMSKY("Perro pomsky"),
    ALUSKY("Alusky"),
    COLLIE("Collie"),
    YORKSHIRE("Yorkshire"),
    PINSCHER("Pinscher"),
    MASTIN("Mastin"),
    PODENCO("Podenco"),
    BRACO("Braco"),
    ALANOESPANNOL("Alano español"),
    DANDIE("Dandie"),
    SHIBA("Shiba"),
    CHOWCHOW("Chow Chow"),
    BORDERCOLLIE("Border Collie"),
    SPANIELBRETON("Spaniel Breton"),
    KEESHOND("Keeshond"),
    SHIKOKUINU("Shikoku inu"),
    BOXER("Boxer"),
    BOBTAIL("Bob Tail"),
    PODENCOIBICENCO("Podenco Ibicenco"),
    POINTERINGLES("Pointer Ingles"),
    FILABRASILENNO("Fila Brasileña"),
    LANDSEER("Land Seer"),
    PODENCOANDALUZ("Podenco Andaluz"),
    COCKAPOO("Cockapoo"),
    PASTORBELGA("Pastor Belga"),
    COLLIEBARBUDO("Collie Barbudo"),
    WELSHCORGI("Welsh Corgi"),
    TERRIERAUSTRALIANO("Terrier Australiano"),
    BRACOALEMAN("Braco Aleman"),
    AMERICANSTAFFORDSHIRE("American Staffordshire"),
    PERROPASTORCROATA("Perro Pastor Croata"),
    PASTORALEMAN("Pastor Aleman"),
    BEAGLE("Beagle");

    private String raza;

    RazasPerro (String raza) {

        this.raza = raza;

    }

    public String getRaza() {
        return raza;
    }
}
