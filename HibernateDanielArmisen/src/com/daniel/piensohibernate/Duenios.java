package com.daniel.piensohibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Duenios {
    private int idduenio;
    private String nombre;
    private String apellidos;
    private String dni;
    private Date fechanacimiento;
    private String pais;
    private DuenioAnimal duenioAnimal;

    @Id
    @Column(name = "idduenio")
    public int getIdduenio() {
        return idduenio;
    }

    public void setIdduenio(int idduenio) {
        this.idduenio = idduenio;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "fechanacimiento")
    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Duenios duenios = (Duenios) o;
        return idduenio == duenios.idduenio &&
                Objects.equals(nombre, duenios.nombre) &&
                Objects.equals(apellidos, duenios.apellidos) &&
                Objects.equals(dni, duenios.dni) &&
                Objects.equals(fechanacimiento, duenios.fechanacimiento) &&
                Objects.equals(pais, duenios.pais);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idduenio, nombre, apellidos, dni, fechanacimiento, pais);
    }

    @ManyToOne
    @JoinColumn(name = "idduenio", referencedColumnName = "idduenio", nullable = false)
    public DuenioAnimal getDuenioAnimal() {
        return duenioAnimal;
    }

    public void setDuenioAnimal(DuenioAnimal duenioAnimal) {
        this.duenioAnimal = duenioAnimal;
    }
}
