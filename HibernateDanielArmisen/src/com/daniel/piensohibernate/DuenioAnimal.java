package com.daniel.piensohibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "duenio_animal", schema = "piensoshibernate", catalog = "")
public class DuenioAnimal {
    private String nombre;
    private Date fechaNac;
    private int id;
    private List<Duenios> duenios;
    private Animal animal;

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fecha_nac")
    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DuenioAnimal that = (DuenioAnimal) o;
        return id == that.id &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(fechaNac, that.fechaNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, fechaNac, id);
    }

    @OneToMany(mappedBy = "duenioAnimal")
    public List<Duenios> getDuenios() {
        return duenios;
    }

    public void setDuenios(List<Duenios> duenios) {
        this.duenios = duenios;
    }

    @ManyToOne
    @JoinColumn(name = "idanimal", referencedColumnName = "idanimal", nullable = false)
    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
