package com.daniel.piensohibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Trabajador {
    private int idtrabajador;
    private double sueldo;
    private String nombre;
    private String apellidos;
    private String dni;
    private Date fechanacimiento;
    private Tienda tienda;
    private List<Venta> ventas;

    @Id
    @Column(name = "idtrabajador")
    public int getIdtrabajador() {
        return idtrabajador;
    }

    public void setIdtrabajador(int idtrabajador) {
        this.idtrabajador = idtrabajador;
    }

    @Basic
    @Column(name = "sueldo")
    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "fechanacimiento")
    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trabajador that = (Trabajador) o;
        return idtrabajador == that.idtrabajador &&
                Double.compare(that.sueldo, sueldo) == 0 &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(apellidos, that.apellidos) &&
                Objects.equals(dni, that.dni) &&
                Objects.equals(fechanacimiento, that.fechanacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idtrabajador, sueldo, nombre, apellidos, dni, fechanacimiento);
    }

    @ManyToOne
    @JoinColumn(name = "idtrabajador", referencedColumnName = "idtienda", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    @OneToMany(mappedBy = "trabajador")
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }
}
